#README

The slides need the font-files to compile. You can comment out some initial font declarations and you should be able compile via `zelatex`.

# Outine of talk


## Polynomials and models of computation

* Models of computation
* Relation between the models
* Classes of polynomials
* Motivation

## General outline of any lower bound proof

* What 'natural' proofs look like
* (optional) Is there a 'natural proofs' barrier? 
* Relevance of depth reduction
  - Monotone circuit LBs
  - Multilinear circuit LBs
  - GKKS1 et al, and [KLSS] + [KS]

## Depth reduction results

* Brent's reduction for formulas
* Hyafil's reduction
* VSBR
* AV, Koiran, Tavenas
* New proof
* Application to homogeneous formulas
* (depending on time) GKKS2